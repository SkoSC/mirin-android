package com.skosc.mirin.lib.android.listener

import android.text.Editable
import android.text.TextWatcher

abstract class SimpleTextWatcher : TextWatcher {
    override fun afterTextChanged(editable: Editable?) = Unit

    override fun beforeTextChanged(sequence: CharSequence?, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(sequence: CharSequence?, start: Int, before: Int, count: Int) = Unit
}