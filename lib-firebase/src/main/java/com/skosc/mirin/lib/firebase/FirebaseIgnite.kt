package com.skosc.mirin.lib.firebase

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FirebaseFirestore
import com.skosc.mirin.core.orThrow
import com.skosc.mirin.lib.di.DefaultModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

class FirebaseIgnite(private val application: Application) {
    companion object {
        private const val KODEIN_MODULE_NAME = "firebase"
    }

    fun ignite(): Kodein.Module {
        val app = FirebaseApp.initializeApp(application).orThrow("Can't instantiate FirebaseApp")

        return DefaultModule(KODEIN_MODULE_NAME) {
            bind<FirebaseApp>() with singleton { app }
            bind<FirebaseFirestore>() with singleton { FirebaseFirestore.getInstance(app) }
        }
    }
}