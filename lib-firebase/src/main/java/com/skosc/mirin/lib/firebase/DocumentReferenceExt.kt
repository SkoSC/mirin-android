package com.skosc.mirin.lib.firebase

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.toObject
import kotlin.coroutines.suspendCoroutine

suspend inline fun <reified T : Any> DocumentReference.request(
    crossinline transformer: (DocumentSnapshot) -> T? = { it.toObject<T>() }
): T = suspendCoroutine { continuation ->
    val task = get()
    task.addOnSuccessListener {
        val result = transformer(it)
        if (result == null) {
            continuation.resumeWith(Result.failure(NullPointerException()))
        } else {
            continuation.resumeWith(Result.success(result))
        }
    }
    task.addOnFailureListener { error ->
        continuation.resumeWith(Result.failure(error))
    }
}