package com.skosc.mirin.feature.main.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.isVisible
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.skosc.mirin.feature.main.R
import com.skosc.mirin.lib.android.BaseActivity
import com.skosc.mirin.lib.android.ScreenStyle
import com.skosc.mirin.lib.android.ScreenStyleApplicator
import com.skosc.mirin.lib.navigation.findNavigator
import kotlinx.android.synthetic.main.feature_main_activity_main.*

class MainActivity : BaseActivity(), ScreenStyleApplicator {
    private val navigator by lazy { nav_host_fragment.findNavigator() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.feature_main_activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemReselectedListener(::onNavigated)
    }

    override fun applyScreenStyle(style: ScreenStyle) {
        if (style.hideToolbar) {
            supportActionBar?.hide()
        } else {
            supportActionBar?.show()
        }

        nav_view.isVisible = !style.hideBottombar
    }

    private fun onNavigated(menuItem: MenuItem) {
        navigator.navigate(menuItem.itemId)
    }
}
