package com.skosc.mirin.lib.navigation

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

class JetPackNavigator(fragment: Fragment) : Navigator {
    private val navController = fragment.findNavController()

    override fun navigate(destination: Int) {
        navController.navigate(destination)
    }
}