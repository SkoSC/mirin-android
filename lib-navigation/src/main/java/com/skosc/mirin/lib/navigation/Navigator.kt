package com.skosc.mirin.lib.navigation

import androidx.annotation.IdRes

interface Navigator {
    fun navigate(@IdRes destination: Int)
}