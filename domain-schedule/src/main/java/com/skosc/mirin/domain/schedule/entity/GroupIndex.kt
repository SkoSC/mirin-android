package com.skosc.mirin.domain.schedule.entity

data class GroupIndex(
    val term: Term,
    val groups: List<String>
)