package com.skosc.mirin.domain.schedule.repo

import com.google.firebase.firestore.FirebaseFirestore
import com.skosc.mirin.domain.schedule.entity.GroupIndex
import com.skosc.mirin.domain.schedule.entity.Term
import com.skosc.mirin.domain.schedule.transformer.GroupIndexTransformer
import com.skosc.mirin.lib.firebase.normalizeAsGroupName
import com.skosc.mirin.lib.firebase.request

class GroupIndexRepo(private val firestore: FirebaseFirestore) {
    companion object {
        private const val FIREBASE_COLLECTION = "group_index"
    }

    suspend fun groupIndex(term: Term): GroupIndex =
        firestore.collection(FIREBASE_COLLECTION).document(term.key).request(GroupIndexTransformer)

    suspend fun searchGroups(term: Term, name: String): List<String> {
        val normalizedName = name.normalizeAsGroupName
        val index = groupIndex(term)
        return index.groups.filter { it.startsWith(normalizedName) }
    }

    suspend fun isQualifiedGroup(term: Term, name: String): Boolean {
        return searchGroups(term, name).size == 1
    }
}