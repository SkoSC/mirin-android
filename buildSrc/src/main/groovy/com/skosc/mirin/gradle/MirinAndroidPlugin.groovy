package com.skosc.mirin.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class MirinAndroidPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.apply("kotlin-android")
        project.pluginManager.apply("kotlin-android-extensions")

        project.android {
            compileSdkVersion 28
            defaultConfig {
                minSdkVersion 21
                targetSdkVersion 28
                versionCode 1
                versionName "1.0"
                testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
            }

            lintOptions {
                checkReleaseBuilds false
            }

            buildTypes {
                release {
                    minifyEnabled false
                    proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
                }

                ci {

                }
            }
        }
    }
}