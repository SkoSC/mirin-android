package com.skosc.mirin.lib.mvvm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.kodein.di.DKodein
import org.kodein.di.Kodein
import org.kodein.di.bindings.Provider
import org.kodein.di.direct
import org.kodein.di.generic
import org.kodein.di.generic.bind
import kotlin.reflect.KClass

class KodeinViewModelProvider<T : ViewModel>(private val kodein: DKodein, private val factory: DKodein.() -> T) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return factory(kodein) as T
    }
}

inline fun <C, reified T : ViewModel> Kodein.BindBuilder.WithContext<C>.viewModelProvider(
    noinline creator: DKodein.() -> T
): Provider<C, ViewModelProvider.Factory> = Provider(contextType, generic()) {
    KodeinViewModelProvider(kodein.direct, creator)
}

fun <T : ViewModel> Kodein.Builder.bindVm(cls: KClass<T>) = bind<ViewModelProvider.Factory>(cls)