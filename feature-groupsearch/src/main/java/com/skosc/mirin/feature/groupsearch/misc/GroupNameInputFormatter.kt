package com.skosc.mirin.feature.groupsearch.misc

import android.widget.EditText
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


class GroupNameInputFormatter {
    private val mask: MaskImpl = MaskImpl.createTerminated(
        arrayOf(
            PredefinedSlots.any(),
            PredefinedSlots.any(),
            PredefinedSlots.any(),
            PredefinedSlots.any(),
            PredefinedSlots.hardcodedSlot('-'),
            PredefinedSlots.any(),
            PredefinedSlots.any(),
            PredefinedSlots.hardcodedSlot('-'),
            PredefinedSlots.any(),
            PredefinedSlots.any()
        )
    )

    fun attach(editText: EditText) {
        val watcher = MaskFormatWatcher(mask)
        watcher.installOn(editText)
    }
}