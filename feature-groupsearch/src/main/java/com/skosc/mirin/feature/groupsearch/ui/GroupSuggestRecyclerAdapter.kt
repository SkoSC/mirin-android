package com.skosc.mirin.feature.groupsearch.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.skosc.mirin.core.Event
import com.skosc.mirin.domain.schedule.entity.Institution
import com.skosc.mirin.feature.groupsearch.R
import com.skosc.mirin.feature.groupsearch.model.GroupUIModel
import com.skosc.mirin.lib.ui.widget.GenericDiffCallback

class GroupSuggestRecyclerAdapter : RecyclerView.Adapter<GroupSuggestRecyclerAdapter.ViewHolder>() {
    private val differ = AsyncListDiffer<GroupUIModel>(this, GenericDiffCallback())
    val onItemClickedEvent: Event<GroupUIModel> = Event()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.feature_groupsearch_item_suggest, parent, false)

        return ViewHolder(view, onItemClickedEvent)
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun getItemId(position: Int): Long {
        return differ.currentList[position].hashCode().toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    fun submit(groups: List<GroupUIModel>) {
        differ.submitList(groups)
    }

    class ViewHolder(view: View, private val onItemClickedEvent: Event<GroupUIModel>) : RecyclerView.ViewHolder(view) {
        private val groupColor: ImageView = view.findViewById(R.id.group_color)
        private val groupName: TextView = view.findViewById(R.id.group_name)
        private val background: View = view.findViewById(R.id.group_item_background)

        fun bind(group: GroupUIModel) {
            val groupColorRes = group.institution.colorRes
            groupName.text = group.name
            groupColor.imageTintList = ContextCompat.getColorStateList(itemView.context, groupColorRes)
            background.setOnClickListener { onItemClickedEvent.fire(group) }
        }
    }
}