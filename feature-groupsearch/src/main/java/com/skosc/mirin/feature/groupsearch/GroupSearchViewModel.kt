package com.skosc.mirin.feature.groupsearch

import androidx.lifecycle.LiveData
import com.skosc.mirin.domain.schedule.entity.GroupName
import com.skosc.mirin.domain.schedule.entity.Term
import com.skosc.mirin.domain.schedule.repo.GroupIndexRepo
import com.skosc.mirin.domain.security.repo.CurrentUserRepository
import com.skosc.mirin.feature.groupsearch.model.GroupUIModel
import com.skosc.mirin.lib.mvvm.BaseViewModel
import com.skosc.mirin.lib.mvvm.LiveEvent
import com.skosc.mirin.lib.mvvm.coroutines.launchLiveData
import com.skosc.mirin.lib.mvvm.coroutines.launchLiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope

abstract class GroupSearchViewModel : BaseViewModel() {
    abstract fun search(query: String): LiveData<List<GroupUIModel>>
    abstract fun selectGroup(group: String): LiveEvent
}

class GroupSearchViewModelImpl(
    private val repo: GroupIndexRepo,
    private val userRepo: CurrentUserRepository
) : GroupSearchViewModel() {
    override fun search(query: String): LiveData<List<GroupUIModel>> = launchLiveData(Dispatchers.IO) {
        repo.searchGroups(Term.current, query)
            .map(GroupUIModel.Companion::create)
            .filter { it.isValid }
            .sortedBy { it.name }
    }

    override fun selectGroup(group: GroupName): LiveEvent = launchLiveEvent(Dispatchers.IO) {
        userRepo.setPrimaryGroup(group)
    }
}