package com.skosc.mirin.feature.groupsearch

import com.skosc.mirin.lib.di.DefaultModule
import com.skosc.mirin.lib.mvvm.bindVm
import com.skosc.mirin.lib.mvvm.viewModelProvider
import org.kodein.di.generic.instance

val ModuleFeatureGroupSearch = DefaultModule("feature_groupsearch") {
    bindVm(GroupSearchViewModel::class) with viewModelProvider { GroupSearchViewModelImpl(instance(), instance()) }
}