package com.skosc.mirin.feature.groupsearch.model

import com.skosc.mirin.domain.schedule.entity.Institution

data class GroupUIModel(val name: String, val institution: Institution) {
    val isValid: Boolean get() = institution != Institution.UNKNOWN

    companion object {
        fun create(group: String): GroupUIModel {
            val institution = Institution.fromGroupName(group)
            return GroupUIModel(group, institution)
        }
    }
}